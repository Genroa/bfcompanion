// import * as zip from "@zip.js/zip.js";
import JSZip from "jszip";
import fs from "fs";

const fileAsBuffer = fs.readFileSync("./public/base.zip");
const blob = new Blob([fileAsBuffer]);
// const reader = new zip.ZipReader(new zip.BlobReader(blob));

// let zipFs = new zip.fs.FS();
// await zipFs.importBlob(blob);

// for (const entry of zipFs.root.children) {
//   console.log(entry.name);

//   if (entry.name.startsWith("images")) {
//     const imageData = await entry.getData(new zip.BlobReader());
//     console.log("BLOB DATA:", imageData);
//   } else {
//     const text = await entry.getData(new zip.TextWriter());
//     console.log("JSON DATA: ", text);
//   }
// }

// Should work in web contexts? For some reason, it doesn't support Node's implementation of Blob.
// const zip = await JSZip.loadAsync(blob);
const zip = await JSZip.loadAsync(fileAsBuffer);

// Read synopsis
console.log(await zip.file("synopsis.json").async("text"));

// Read definitions
const definitions = zip.filter((path, entry) => path.startsWith("definitions"));
for (const entry of definitions) {
  console.log(entry.name, JSON.parse(await entry.async("text")));
}

// Read images
const images = zip.filter((path, entry) => path.startsWith("images"));
for (const entry of images) {
  console.log(entry.name, await entry.async("blob"));
}
