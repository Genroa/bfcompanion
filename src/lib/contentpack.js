import JSZip from "jszip";
import { directories, isOPFSSupported, readFile, readdir, walk } from "./opfs";
import { useCompendium } from "src/stores/compendium";
import { storeToRefs } from "pinia";

function getLocalStoragePacks() {
  return JSON.parse(localStorage.getItem("bfc:contentpacks") || "[]");
}

export async function listContentPackUUIds() {
  if (!isOPFSSupported()) {
    return getLocalStoragePacks().map((o) => o.UUID);
  }

  const dirs = await readdir("content");
  console.log(
    "Content pack folders:",
    [...dirs.keys()].map((s) => s.substring(8))
  );
  return [...dirs.keys()].map((s) => s.substring(8));
}

export async function getContentPackArchive(dependency) {
  if (!isOPFSSupported()) {
    return null;
  }

  return new Blob([
    await readFile(`content/${dependency.UUID}/${dependency.filename}`),
  ]);
}

export async function getContentPackDependencyFile(UUID) {
  if (!isOPFSSupported()) {
    return getLocalStoragePacks().find((o) => o.UUID === UUID);
  }

  const buffer = await readFile(`content/${UUID}/dependency.json`);
  const decoder = new TextDecoder();
  return JSON.parse(decoder.decode(buffer));
}

export async function readContentPack(blob) {
  try {
    return await JSZip.loadAsync(blob);
  } catch (err) {
    throw new Error(
      "Failed to read the given blob. Either it isn't a zip file, or it is of a kind I cannot understand."
    );
  }
}

export async function getSynopsis(contentPack) {
  try {
    return JSON.parse(await contentPack.file("synopsis.json").async("text"));
  } catch (err) {
    throw new Error(
      "Failed to find/read a synopsis.json file at the root fo the zip file"
    );
  }
}

export async function listDefinitionFiles(contentPack) {
  return contentPack.filter(
    (path, entry) => path.startsWith("definitions") && path.endsWith(".json")
  );
}

export async function listImageFiles(contentPack) {
  return contentPack.filter(
    (path, entry) => path.startsWith("images") && path.endsWith(".png")
  );
}

export async function getDefinitionFile(entry) {
  return JSON.parse(await entry.async("text"));
}

export async function getImageFile(entry) {
  return await entry.async("blob");
}

export async function getImageFileURL(entry) {
  const imageBlob = await getImageFile(entry);
  return window.URL.createObjectURL(imageBlob);
}

export function getValidCopyName(name) {
  const compendium = useCompendium();
  const { knownContentPacks } = storeToRefs(compendium);
  let i = 1;
  let copyName = `${name}_copy_${i}`;
  while (
    Object.values(knownContentPacks.value).find((s) => s.name === copyName)
  ) {
    i++;
    copyName = `${name}_copy_${i}`;
  }
  return copyName;
}
