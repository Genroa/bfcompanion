function normalize(path) {
  return path.trim().replace(/\/+/g, "/").replace(/^\//, "").replace(/\/$/, "");
}
function basename(path) {
  return directories(path).pop() || "";
}
function dirname(path) {
  return directories(path).slice(0, -1).join("/") || "";
}
function directories(path) {
  return path
    .split("/")
    .map((dir) => dir.trim())
    .filter((dir) => dir !== "");
}

async function _chdir(parent, path) {
  const name = path.shift();
  if (!name) {
    return parent;
  }
  const child = await parent.getDirectoryHandle(name);
  const dir = await _chdir(child, path);
  return dir;
}
async function chdir(path) {
  const root = await navigator.storage.getDirectory();
  return _chdir(root, directories(normalize(path)));
}

async function _mkdir(parent, path) {
  const name = path.shift();
  if (!name) {
    return parent;
  }
  const child = await parent.getDirectoryHandle(name, { create: true });
  const dir = await _mkdir(child, path);
  return dir;
}
async function mkdir(path) {
  const root = await navigator.storage.getDirectory();
  return _mkdir(root, directories(normalize(path)));
}

async function readdir(pathOrHandle) {
  const entries = new Map();
  const root = await navigator.storage.getDirectory();
  const dir =
    typeof pathOrHandle === "string" ? await chdir(pathOrHandle) : pathOrHandle;
  for await (const handle of dir.values()) {
    const path = await root.resolve(handle);
    if (!path) {
      console.warn(
        `Failed to resolve path of file ${handle.name} from ${root.name}`
      );
      continue;
    }
    entries.set(`${path.join("/")}`, handle);
  }
  return entries;
}

async function resolveFileFromPath(path) {
  const filepath = normalize(path);
  const filename = basename(filepath);
  const directory = await chdir(dirname(filepath));
  const handle = await directory.getFileHandle(filename);
  return handle;
}
async function getParentDirectory(handle) {
  const root = await navigator.storage.getDirectory();
  const path = await root.resolve(handle);
  if (!path) {
    throw new Error("failed to resolve file");
  }
  if (path.length < 2) {
    return root;
  }
  path.pop();
  const directory = await chdir(path.join("/"));
  return directory;
}

async function rmdir(pathOrHandle) {
  try {
    const directory =
      typeof pathOrHandle === "string"
        ? await chdir(pathOrHandle)
        : pathOrHandle;
    const parent = await getParentDirectory(directory);
    await parent.removeEntry(directory.name, { recursive: true });
    return true;
  } catch (error) {
    if (error instanceof DOMException && error.name === "NotFoundError") {
      return true;
    }
    console.error(
      `The following error occured while trying to remove directory ${pathOrHandle}: ${error}`
    );
    return false;
  }
}

async function readFile(pathOrHandle) {
  const handle =
    typeof pathOrHandle === "string"
      ? await resolveFileFromPath(pathOrHandle)
      : pathOrHandle;
  const file = await handle.getFile();
  return file.arrayBuffer();
}

async function stat(filepath) {
  const handle = await resolveFileFromPath(filepath);
  const file = await handle.getFile();
  return file;
}

async function removeFile(pathOrHandle) {
  try {
    const file =
      typeof pathOrHandle === "string"
        ? await resolveFileFromPath(pathOrHandle)
        : pathOrHandle;
    const directory = await getParentDirectory(file);
    await directory.removeEntry(file.name, { recursive: false });
    return true;
  } catch (error) {
    if (error instanceof DOMException && error.name === "NotFoundError") {
      return true;
    }
    console.error(
      `The following error occured while trying to remove file ${pathOrHandle}: ${error}`
    );
    return false;
  }
}

async function writeFile(filepath, data) {
  const path = normalize(filepath);
  const dir = await mkdir(dirname(path));
  const file = await dir.getFileHandle(basename(path), { create: true });
  const stream = await file.createWritable();
  await stream.write(data);
  await stream.close();
  return file;
}

function formatBytes(bytes) {
  const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes == 0) {
    return "0 Bytes";
  }
  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  if (i == 0) {
    return bytes + " " + sizes[i];
  }
  return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
}
async function statistics() {
  const estimate = await navigator.storage.estimate();
  const { usage = 0, quota = 0 } = estimate;
  const percent = quota === 0 ? 0 : ((usage / quota) * 100).toFixed(2);
  return {
    percent: `${percent}%`,
    usage: formatBytes(usage),
    quota: formatBytes(quota),
  };
}

async function walk(root, parent) {
  const dir = parent ?? root;
  const entries = new Map();
  for await (const handle of dir.values()) {
    const path = await root.resolve(handle);
    if (!path) {
      console.warn(
        `Failed to resolve path of file ${handle.name} from ${root.name}`
      );
      continue;
    }
    entries.set(`${path.join("/")}`, handle);
    if (handle.kind === "directory") {
      const map = await walk(root, handle);
      for (const [path, handle] of map) {
        entries.set(path, handle);
      }
    }
  }
  return entries;
}

async function breadcrumb(pathOrHandle) {
  const crumbs = [];
  const root = await navigator.storage.getDirectory();
  let dirs = [];
  if (typeof pathOrHandle === "string") {
    dirs = directories(normalize(pathOrHandle));
  } else {
    const path = await root.resolve(pathOrHandle);
    if (path) {
      dirs = path;
    }
  }
  crumbs.push(root);
  let parent = root;
  for (const dir of dirs) {
    const child = await parent.getDirectoryHandle(dir);
    crumbs.push(child);
    parent = child;
  }
  return crumbs;
}

function isOPFSSupported() {
  return navigator.storage !== undefined;
}

export {
  directories,
  breadcrumb,
  chdir,
  mkdir,
  readFile,
  readdir,
  removeFile,
  rmdir,
  stat,
  statistics,
  walk,
  writeFile,
  isOPFSSupported,
};
