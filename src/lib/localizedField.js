import { storeToRefs } from "pinia";
import i18n, { tfallbacknull } from "src/i18n";
import { useCompendium } from "src/stores/compendium";
import { useEditStore } from "src/stores/edit";
import { computed, ref } from "vue";
import { debounce } from "quasar";
import { put } from "src/lib/put";

const { locale } = i18n.global;
const compendium = useCompendium();
const { texts, textsMapping } = storeToRefs(compendium);
const editStore = useEditStore();
const { selectedDefinitionFullName, dependency, files } =
  storeToRefs(editStore);

export function useLocalizedField(key) {
  const localizedFieldString = computed(() => {
    console.log("Localisation key=", key, tfallbacknull(key));
    return tfallbacknull(key);
  });

  const bufferedFieldValue = ref(localizedFieldString.value);
  const updateLocalizedFieldDebounced = debounce(updateLocalizedField, 700);

  function updateLocalizedField() {
    // console.log("Should update path:", key, bufferedFieldValue.value);

    // Deepset the texts we loaded for this file, then reload the compendium - but only the texts part.
    // This is necessary to respect the loading order and external strings potentially overriding this one.
    // EDIT: nope, only editing texts and the local texts of the file is enough, because if it isn't a local loc string, we forbid to edit it.
    put(texts.value, `${locale.value}.${key}`, bufferedFieldValue.value);
    put(
      files.value[selectedDefinitionFullName.value].texts,
      `${locale.value}.${key}`,
      bufferedFieldValue.value
    );
  }

  function bufferNewLocalizedField(newValue) {
    bufferedFieldValue.value = newValue;
    updateLocalizedFieldDebounced();
  }

  function applyNewLocalizedField() {
    console.log(bufferedFieldValue.value, localizedFieldString.value);
    if (bufferedFieldValue.value !== localizedFieldString.value)
      updateLocalizedField();
  }

  function createTextEntry(key) {
    textsMapping.value[`${locale.value}.${key}`] = {
      contentPack: dependency.value.filename,
      file: selectedDefinitionFullName.value,
    };

    put(texts.value, `${locale.value}.${key}`, "<>");
    put(
      files.value[selectedDefinitionFullName.value].texts,
      `${locale.value}.${key}`,
      "<>"
    );

    bufferedFieldValue.value = "<>";
  }

  return {
    localizedFieldString,
    bufferNewLocalizedField,
    applyNewLocalizedField,
    createTextEntry,
  };
}
