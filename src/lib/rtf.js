import { getSpecialWeaponRuleName } from "src/i18n";

const SQUARE_SYMBOL = [
  /(?:\[SQUARE\])/g,
  '<img class="inline-icon" src="symbols/square_miss.png"/>',
];

const TRIANGLE_SYMBOL = [
  /(?:\[TRIANGLE\])/g,
  '<img class="inline-icon" src="symbols/triangle_miss.png"/>',
];

const PENTAGON_SYMBOL = [
  /(?:\[PENTAGON\])/g,
  '<img class="inline-icon" src="symbols/pentagon_miss.png"/>',
];

const DISCOVERY_SYMBOL_1 = [
  /(?:\[GROUP1\])/g,
  '<img class="inline-icon" src="symbols/discovery_1.png"/>',
];

const DISCOVERY_SYMBOL_2 = [
  /(?:\[GROUP2\])/g,
  '<img class="inline-icon" src="symbols/discovery_2.png"/>',
];

const DISCOVERY_SYMBOL_3 = [
  /(?:\[GROUP3\])/g,
  '<img class="inline-icon" src="symbols/discovery_3.png"/>',
];

const DISCOVERY_SYMBOL_4 = [
  /(?:\[GROUP4\])/g,
  '<img class="inline-icon" src="symbols/discovery_4.png"/>',
];

const MARKDOWN_BOLD = [/\*{2}([\w\W]*?)\*{2}/g, "<b>$1</b>"];
const MARKDOWN_ITALIC = [/\*([\w\W]*?)\*/g, "<i>$1</i>"];

const SPECIAL_WEAPON_RULE = (profile) => [
  /(?:\[WRULE\|)([\w\W]*?)\]/g,
  (_, specialRuleId) =>
    `<u>${getSpecialWeaponRuleName(profile, specialRuleId)}</u>`,
];

export function parseRichText(text, context) {
  if (!text) return text;
  text = new String(text)
    .replace(...MARKDOWN_BOLD)
    .replace(...MARKDOWN_ITALIC)
    .replace(...SQUARE_SYMBOL)
    .replace(...TRIANGLE_SYMBOL)
    .replace(...PENTAGON_SYMBOL)
    .replace(...DISCOVERY_SYMBOL_1)
    .replace(...DISCOVERY_SYMBOL_2)
    .replace(...DISCOVERY_SYMBOL_3)
    .replace(...DISCOVERY_SYMBOL_4);

  if (context) {
    // These next replacements will use actual knowledge of the context ("this is a special weapon rule" needs to know the hostile profile we're talking about to find the right text), as well as i18n $t. They're more expensive, so if no context is provided, don't bother trying to apply any of these.
    if (context.profile)
      text = text.replace(...SPECIAL_WEAPON_RULE(context.profile));
  }

  return text;
}
