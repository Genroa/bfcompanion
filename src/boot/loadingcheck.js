import { boot } from "quasar/wrappers";
import { useCompendium } from "src/stores/compendium";

export default boot(({ urlPath, redirect }) => {
  const compendium = useCompendium();
  if (!urlPath.endsWith("/loading") && !compendium.isLoaded) {
    // We're loading a URL that isn't /loading, but the compendium is empty. Forcing /loading redirection.
    redirect({ name: "loading" });
  }
});
