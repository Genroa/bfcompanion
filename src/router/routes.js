import { storeToRefs } from "pinia";
import { SAVE_STATUS, useSaveStore } from "src/stores/save";
import CombatView from "pages/CombatView.vue";

const mustHaveSelectedSave = (to, from) => {
  const saveStore = useSaveStore();
  const { selectedSave } = storeToRefs(saveStore);
  if (selectedSave.value === null) {
    console.log("Selected save:", selectedSave);
    return { name: "saves" };
  }
};

const redirectToStateScreen = (to, from) => {
  const saveStore = useSaveStore();
  const { selectedSave } = storeToRefs(saveStore);

  if (selectedSave.value.status === SAVE_STATUS.IN_EXPEDITION)
    return { name: "expedition" };

  if (selectedSave.value.status === SAVE_STATUS.IN_COMBAT)
    return { name: "combat" };
};

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("pages/PrecipiceView.vue"),
    beforeEnter: [mustHaveSelectedSave, redirectToStateScreen],
  },

  {
    path: "/expedition",
    name: "expedition",
    component: () => import("pages/ExpeditionView.vue"),
    beforeEnter: [mustHaveSelectedSave],
  },

  {
    path: "/combat",
    name: "combat",
    component: CombatView,
    beforeEnter: [mustHaveSelectedSave],
  },

  {
    path: "/loading",
    name: "loading",
    component: () => import("pages/LoadingView.vue"),
  },

  {
    path: "/saves",
    name: "saves",
    component: () => import("pages/SaveSelection.vue"),
  },

  {
    path: "/content",
    name: "managecontent",
    component: () => import("pages/ManageContent.vue"),
  },

  {
    path: "/content/edit",
    name: "editcontentpack",
    component: () => import("pages/EditContentPack.vue"),
  },

  {
    path: "/content/edit/definition",
    name: "editdefinitionfile",
    component: () => import("pages/EditDefinitionFile.vue"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    name: "error",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
