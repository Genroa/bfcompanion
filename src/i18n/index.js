import { storeToRefs } from "pinia";
import { useCompendium } from "src/stores/compendium";
import { createI18n } from "vue-i18n";

export const defaultMessages = {
  en: {
    ui: {
      loading: "Loading...",
    },
  },
};

const i18n = createI18n({
  locale: localStorage.getItem("bfc:locale") || "en",
  fallbackLocale: "en",
  legacy: false,
  messages: defaultMessages,
  silentTranslationWarn: true,
});

export function tfallbacknull(...keys) {
  for (const key of keys) {
    if (i18n.global.te(key)) return i18n.global.t(key);
  }
  console.warn("Couldn't find any valid loc key to use among:", keys);
  return null;
}

export function tfallback(...keys) {
  for (const key of keys) {
    if (i18n.global.te(key)) return i18n.global.t(key);
  }
  console.warn("Couldn't find any valid loc key to use among:", keys);
  return keys[0];
}

function inspiredFallback(profileId, keyFunc) {
  const keys = [keyFunc(profileId)];
  if (profileId.endsWith("__inspired")) {
    keys.push(keyFunc(profileId.slice(0, -10)));
  }
  return keys;
}

export function getSpecialWeaponRuleName(profileId, specialRuleId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.weaponRules.${specialRuleId}.name`
    ),
    `hostileProfiles.${profileId}.weaponRules.${specialRuleId}.name`,
    `weaponRules.${specialRuleId}.name`
  );
}

export function getSpecialWeaponRuleDescription(profileId, specialRuleId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.weaponRules.${specialRuleId}.description`
    ),
    `hostileProfiles.${profileId}.weaponRules.${specialRuleId}.description`,
    `weaponRules.${specialRuleId}.description`
  );
}

export function getWeaponActionName(profileId, weaponActionId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.weaponActions.${weaponActionId}`
    ),
    `hostileProfiles.${profileId}.weaponActions.${weaponActionId}`,
    `weaponActions.${weaponActionId}`
  );
}

export function getUniqueActionName(profileId, uniqueActionId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.uniqueActions.${uniqueActionId}.name`
    ),
    `hostileProfiles.${profileId}.uniqueActions.${uniqueActionId}.name`,
    `uniqueActions.${uniqueActionId}.name`
  );
}

export function getUniqueBehaviourName(profileId, uniqueBehaviourId) {
  return tfallback(
    `hostileProfiles.${profileId}.uniqueBehaviours.${uniqueBehaviourId}.name`,
    `behaviours.${uniqueBehaviourId}.name`
  );
}

export function getUniqueBehaviourDescription(profileId, uniqueBehaviourId) {
  return tfallback(
    `hostileProfiles.${profileId}.uniqueBehaviours.${uniqueBehaviourId}.description`,
    `behaviours.${uniqueBehaviourId}.description`
  );
}

export function getBehaviourSituationName(profileId, situationId) {
  return tfallback(
    `hostileProfiles.${profileId}.behaviourSituations.${situationId}.name`,
    `behaviourSituations.${situationId}.name`
  );
}
export function getBehaviourSituationDescription(profileId, situationId) {
  return tfallback(
    `hostileProfiles.${profileId}.behaviourSituations.${situationId}.description`,
    `behaviourSituations.${situationId}.description`
  );
}

export function getUniqueActionDescription(profileId, uniqueActionId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.uniqueActions.${uniqueActionId}.description`
    ),
    `hostileProfiles.${profileId}.uniqueActions.${uniqueActionId}.description`,
    `uniqueActions.${uniqueActionId}.description`
  );
}

export function getSpecialRuleName(profileId, specialRuleId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.specialRules.${specialRuleId}.name`
    ),
    `hostileProfiles.${profileId}.specialRules.${specialRuleId}.name`,
    `specialRules.${specialRuleId}.name`
  );
}

export function getSpecialRuleDescription(profileId, specialRuleId) {
  return tfallback(
    ...inspiredFallback(
      profileId,
      (profileId) =>
        `explorerProfiles.${profileId}.specialRules.${specialRuleId}.description`
    ),
    `hostileProfiles.${profileId}.specialRules.${specialRuleId}.description`,
    `specialRules.${specialRuleId}.description`
  );
}

export function getProfileProperty(profileId, property) {
  const keys = [`explorerProfiles.${profileId}.${property}`];
  if (profileId.endsWith("__inspired"))
    keys.push(`explorerProfiles.${profileId.slice(0, -10)}.${property}`);
  else keys.push(`hostileProfiles.${profileId}.${property}`);
  return tfallback(...keys);
}

export const getProfileName = (profileId) =>
  getProfileProperty(profileId, "name");
export const getExplorerTitle = (profileId) =>
  getProfileProperty(profileId, "title");
export const getExplorerArt = (profileId) => {
  const compendium = useCompendium();
  const { images } = storeToRefs(compendium);
  profileId = profileId.endsWith("__inspired")
    ? profileId.slice(0, -10)
    : profileId;
  return images.value[`explorer.${profileId}`];
};

export const getExplorerSecretAgenda = (profileId) =>
  getProfileProperty(profileId, "secretAgenda");

export const getHostileArt = (profileId) => {
  const compendium = useCompendium();
  const { images } = storeToRefs(compendium);
  return images.value[`hostile.${profileId}`];
};

export function getHostileGroupLabel({ id, profiles }) {
  let label = i18n.global.t(`hostileGroups.${id}`);
  if (label === `hostileGroups.${id}`) {
    label = profiles
      .map((p) => tfallback(`hostileGroups.${p}`, `hostileProfiles.${p}.name`))
      .join(" & ");
  }
  return label;
}

export default i18n;
