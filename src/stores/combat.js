import { defineStore, storeToRefs } from "pinia";
import { COMBAT_PHASES } from "src/lib/combat_phases";
import { useSaveStore } from "./save";
import { useCompendium } from "./compendium";

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

export const useCombatStore = defineStore("combat", {
  persist: {
    key: "bfc:combat",
  },
  state: () => ({
    participatingHostiles: [
      "traitor_guardsman",
      "traitor_guardsman",
      "traitor_guardsman",
    ],
    participatingExplorerIds: [
      "dahyak_grekh",
      "espern_locarno",
      "pious_vorne",
      "rain_and_raus",
    ],
    initiativeTrack: [],
    activeFighterIndex: null,
    currentCombatPhase: "",
    behaviourBlackstoneDice: 0,
    displayEventRollDialog: false,
    displayManoeuverDialog: false,
    currentEncounterType: "default",
  }),

  getters: {
    fightingExplorers(state) {
      const { party } = storeToRefs(useSaveStore());
      return party.value.filter((h) =>
        state.participatingExplorerIds.includes(h.id)
      );
    },

    activeFighter(state) {
      if (state.activeFighterIndex === null) return null;
      return state.initiativeTrack[state.activeFighterIndex];
    },

    activeFighterProfiles() {
      if (!this.activeFighter) return [];

      const { explorerProfiles, hostileGroups, hostileProfiles } = storeToRefs(
        useCompendium()
      );

      if (this.activeFighter.type === "explorer")
        return explorerProfiles[this.activeFighter.id];

      return hostileGroups[this.activeFighter.id].profiles.map(
        (id) => hostileProfiles[id]
      );
    },
  },

  actions: {
    loadNewRound(startOfFight) {
      this.currentCombatPhase = startOfFight
        ? "before_combat"
        : "cover_fire_and_manoeuver";

      this.shuffleInitiativeTrack();
    },
    shuffleInitiativeTrack() {
      const compendium = useCompendium();
      const { hostileGroups } = storeToRefs(compendium);

      const explorersFightData = this.participatingExplorerIds.map((id) => ({
        id,
        profile: id,
        type: "explorer",
      }));
      const hostilesFightData = this.participatingHostiles.map((gid, index) => {
        console.log("GID", gid, hostileGroups.value[gid]);
        return {
          id: index + 1,
          profiles: hostileGroups.value[gid].profiles,
          profile: gid,
          type: "hostile",
          group: index + 1,
        };
      });

      const track = [...explorersFightData, ...hostilesFightData];
      shuffle(track);
      this.initiativeTrack = track;
      this.activeFighterIndex = null;
    },

    activateNextFighter() {
      this.behaviourBlackstoneDice = 0;

      let nextFighter = this.activeFighterIndex + 1;
      if (nextFighter >= this.initiativeTrack.length) nextFighter = null;

      this.activeFighterIndex = nextFighter;
    },

    goToNextPhase(executePhase = false) {
      const index = COMBAT_PHASES.findIndex(
        (phase) => phase === this.currentCombatPhase
      );
      this.currentCombatPhase = COMBAT_PHASES[index + 1];
      if (executePhase) this.executeCurrentPhase();
    },

    executeBeforeCombatPhase() {},

    executeCoverFireAndManoeuverPhase() {
      // display stuff
      this.activeFighterIndex = 0;
    },

    executeActivationPhase() {
      this.activateNextFighter();
    },

    executeEventPhase() {
      this.loadNewRound(false);
    },

    executionAfterCombatPhase() {},

    executeCurrentPhase() {
      switch (this.currentCombatPhase) {
        case "before_combat":
          this.executeBeforeCombatPhase();
          break;

        case "cover_fire_and_manoeuver":
          this.executeCoverFireAndManoeuverPhase();
          break;

        case "activations":
          this.executeActivationPhase();
          break;

        case "event":
          this.executeEventPhase();
          break;

        case "after_combat":
          this.executionAfterCombatPhase();
          break;
      }
    },
  },
});
