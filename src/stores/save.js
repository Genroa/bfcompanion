import { defineStore, storeToRefs } from "pinia";
import { v4 as uuid } from "uuid";
import { useCompendium } from "./compendium";

export const SAVE_STATUS = {
  PLANNING_EXPEDITION: "PLANNING_EXPEDITION",
  IN_EXPEDITION: "IN_EXPEDITION",
  IN_COMBAT: "IN_COMBAT",
};

export const useSaveStore = defineStore("saves", {
  persist: {
    key: "bfc:saves",
    serializer: {
      serialize: (state) => {
        if (state.selectedSave !== null) {
          // Do not edit the original selectedSave ref, or you'll break everything. Here, a shallow copy is good enough.
          state = { ...state };
          state.selectedSave = state.selectedSave.id;
        }
        return JSON.stringify(state);
      },
      deserialize: (string) => {
        let state = JSON.parse(string);
        if (state.selectedSave !== null)
          state.selectedSave = state.saves.find(
            (s) => s.id === state.selectedSave
          );
        return state;
      },
    },
  },
  state: () => ({
    saves: [],
    selectedSave: null,
  }),
  getters: {
    party: (state) => {
      if (!state.selectedSave) return [];
      const { explorerProfiles } = storeToRefs(useCompendium());
      return state.selectedSave.party.map(
        (explorerId) => explorerProfiles.value[explorerId]
      );
    },
  },
  actions: {
    createNewSave() {
      this.saves.push({
        id: uuid(),
        name: `Save ${this.saves.length + 1}`,
        deadExplorers: [],
        inspiredExplorers: [],
        playedExplorers: [
          "janus_draik",
          "amallyn_shadowguide",
          "pious_vorne",
          "thaddeus_the_purifier",
          "ur_025",
          "dahyak_grekh",
          "espern_locarno",
          "rain_and_raus",
        ],
        party: [],
        status: SAVE_STATUS.PLANNING_EXPEDITION,
      });
    },
  },
});
