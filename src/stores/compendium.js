import { defineStore } from "pinia";
import * as fs from "src/lib/opfs";
import JSZip from "jszip";
import deepmerge from "deepmerge";
import i18n from "src/i18n";
import { getLangNameFromCode } from "language-name-map";
import {
  getContentPackArchive,
  getContentPackDependencyFile,
  getDefinitionFile,
  getImageFileURL,
  getSynopsis,
  listContentPackUUIds,
  listDefinitionFiles,
  listImageFiles,
  readContentPack,
} from "src/lib/contentpack";

export const overwriteMergeRule = {
  arrayMerge: (destinationArray, sourceArray, options) => sourceArray,
};

export const CONTENT_PACK_STATE = {
  STORED: "STORED",
  MISSING_FILE: "MISSING_FILE",
  FAILED_TO_FETCH: "FAILED_TO_FETCH",
  FAILED_TO_LOAD: "FAILED_TO_LOAD",
};

export const DEPENDENCY_TYPE = {
  URL: "URL",
  FILE: "FILE",
};

async function entryExists(path, entryName) {
  if (!fs.isOPFSSupported()) return false;

  const folder = await fs.readdir(path);
  const folderEntries = Array.from(folder.keys()).map((s) =>
    s.substring(path.length)
  );
  return folderEntries.includes(entryName);
}

function formatBytes(bytes) {
  const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes == 0) {
    return "0 Bytes";
  }
  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  if (i == 0) {
    return bytes + " " + sizes[i];
  }
  return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
}

function getCleanFileNameFromZip(name) {
  const split = name.split(/\\/);
  const nameWithExtension = split[split.length - 1];
  const splitDot = nameWithExtension.split(".");
  splitDot.pop();
  return splitDot.join(".");
}

export const useCompendium = defineStore("compendium", {
  state: () => ({
    limitedMode: false,
    shouldUpdateContentPacks: false,
    shouldStartupOnManagement: false,
    currentLoadingStep: "Loading...",
    currentLoadingSubstep: "",
    loadingIssuesList: [],

    // UUID => filename, allowing us to loop over everything
    contentPackUUIDS: {},
    knownContentPacks: {},

    // Profiles
    explorerProfiles: {},
    hostileProfiles: {},

    // Specific combinations of hostiles you can encounter
    hostileGroups: {},
    encounterTypes: {},

    // Loaded images
    images: {},

    // i18n messages
    texts: {},
    textsMapping: {},

    styles: {},
  }),
  getters: {
    // Check if any dependency is loaded. If not, this is a fresh loading.
    isLoaded: (state) => Object.keys(state.knownContentPacks).length > 0,
    noninspiredExplorers: (state) =>
      Object.fromEntries(
        Object.entries(state.explorerProfiles).filter(
          ([id, profile]) => !id.endsWith("__inspired")
        )
      ),

    availableLanguages: (state) =>
      Object.keys(state.texts).map((code) => ({
        label: getLangNameFromCode(code).native,
        value: code,
      })),
  },
  actions: {
    setLocale(locale) {
      this.selectedLanguage = locale;
      console.log(i18n.global);
    },
    async loadCompendium() {
      if (!fs.isOPFSSupported()) {
        this.limitedMode = true;
        console.log(
          "Limited mode enabled (OPFS not supported on this device)."
        );
      } else {
        console.log(
          "Limited mode disabled. OPFS API is supported on this device."
        );
        // 1. Look if /content exists. If it doesn't, create the folder structure.
        if (!(await entryExists("", "content"))) {
          console.log("Didn't find the content folder. Creating it...");
          await fs.mkdir("content");
        }
      }

      const contentPackUUIDS = await listContentPackUUIds();

      if (!contentPackUUIDS.includes("base")) {
        console.log(
          "Didn't find the base content pack. Creating its folder and dependency file."
        );
        const baseDependency = {
          UUID: "base",
          filename: "base.zip",
          type: DEPENDENCY_TYPE.URL,
          url: "base.zip",
          name: "Base Content",
          version: 1,
          enabled: true,
        };
        await fs.mkdir("content/base");
        await fs.writeFile(
          "content/base/dependency.json",
          JSON.stringify(baseDependency, undefined, 2)
        );

        const packs = JSON.parse(
          localStorage.getItem("bfc:contentpacks") || "[]"
        );
        packs.push(baseDependency);
        console.log(packs);
        localStorage.setItem("bfc:contentpacks", JSON.stringify(packs));

        contentPackUUIDS.push("base");
      }

      const knownContentPacks = [];
      for (const UUID of contentPackUUIDS) {
        try {
          const dependency = await getContentPackDependencyFile(UUID);
          contentPackUUIDS[UUID] = dependency;
          knownContentPacks.push(dependency);
        } catch (err) {
          console.log(err);
          console.log(
            "Failed to load content pack",
            UUID,
            ": dependency file is probably missing."
          );
        }
      }
      console.log(
        "Compiled dependencies. Now trying to load content packs: ",
        knownContentPacks
      );

      const updatedKnownContentPacks = [];
      for (const dependency of knownContentPacks) {
        try {
          if (!dependency.enabled) {
            console.log(
              "Dependency",
              dependency.name,
              "is disabled. Skipping."
            );
            continue;
          }

          this.currentLoadingStep = `Loading content pack '${JSON.stringify(
            dependency.name
          )}'...`;
          console.log("Loading dependency:", dependency);
          // Try to load each known content pack. If not found locally and they're a pack we originally downloaded from a URL, try to fetch it again.
          const updatedContentPackSynopsis = await this.tryLoadingContentPack(
            dependency,
            this.shouldUpdateContentPacks
          );
          updatedKnownContentPacks.push(updatedContentPackSynopsis);
        } catch (err) {
          console.log("Loading err:", err);
        }
      }
      this.knownContentPacks = updatedKnownContentPacks;

      // Update i18n after loading the content texts
      for (const locale of Object.keys(this.texts)) {
        i18n.global.mergeLocaleMessage(locale, this.texts[locale]);
      }

      // For each Explorer with no inspired version defined, generate an identical twin version just so the system doesn't complain
      for (const [profileId, profile] of Object.entries(
        this.explorerProfiles
      )) {
        if (profileId.endsWith("__inspired")) continue;
        const inspiredProfileId = `${profileId}__inspired`;

        if (this.explorerProfiles[inspiredProfileId]) continue;

        this.explorerProfiles[inspiredProfileId] = {
          ...profile,
          id: inspiredProfileId,
        };
        this.texts.en.explorerProfiles[inspiredProfileId] = {
          name: `${this.texts.en.explorerProfiles[profileId].name} (inspired)`,
        };
      }

      if (fs.isOPFSSupported()) {
        localStorage.setItem(
          "bfc:contentpacks",
          JSON.stringify(this.knownContentPacks)
        );
      }

      setTimeout(() => {
        this.router.push({
          name: this.shouldStartupOnManagement ? "managecontent" : "home",
        });
        this.shouldUpdateContentPacks = false;
        this.shouldStartupOnManagement = false;
      }, 1000);
    },

    async tryLoadingContentPack(dependency, updateContentPack = false) {
      const { UUID, filename, name, type, version, url } = dependency;
      let zipBlob = null;
      const failedToLoadSynopsis = {
        ...dependency,
        size: "???",
        state: CONTENT_PACK_STATE.FAILED_TO_LOAD,
      };

      this.currentLoadingSubstep = `Looking for saved content pack '${filename}'...`;
      console.log(`Looking for saved content pack '${filename}'...`);

      // If the file couldn't be found locally, or it is a URL based file and we're trying to update content packs: try to fetch it again
      if (
        !(await entryExists(`content/${UUID}/`, `${filename}`)) ||
        (updateContentPack && type === "URL")
      ) {
        console.log(
          `Didn't find content pack '${filename}' locally, or we're updating URL content packs.`
        );

        if (type !== "URL") {
          console.log(
            `Content pack '${filename}' isn't a URL dependency, I can't fetch it again. Skipping.`
          );
          return {
            ...dependency,
            size: "???",
            state: CONTENT_PACK_STATE.MISSING_FILE,
          };
        }
        this.currentLoadingSubstep = `Downloading content pack '${filename}'...`;
        try {
          const res = await fetch(url);
          zipBlob = await res.blob();
        } catch (err) {
          console.log("ERR:", err);
          return {
            ...dependency,
            name: filename,
            size: 0,
            state: CONTENT_PACK_STATE.FAILED_TO_FETCH,
          };
        }
        console.log("Downloaded zip:", zipBlob);
      } else {
        zipBlob = new Blob([await fs.readFile(`content/${UUID}/${filename}`)]); // the Blob conversion is really only to keep a consistent type between both means of obtaining it
      }

      try {
        // Now parse its content with JSZip.
        this.currentLoadingSubstep = `Loading content pack '${filename}'...`;
        const contentPack = await readContentPack(zipBlob);

        // Read definitions
        for await (const entry of await listDefinitionFiles(contentPack)) {
          await this.tryLoadingDefinitionFile(filename, entry);
        }

        // Read images
        for await (const entry of await listImageFiles(contentPack)) {
          await this.tryLoadingImageFile(entry);
        }

        // Store the good looking zip file in the OPFS!
        if (!(await entryExists(`content/`, UUID))) {
          console.log(
            `Content pack folder '${UUID}' not found. Creating it...`
          );
          await fs.mkdir("content/" + UUID);
        }
        if (
          !(await entryExists(`content/${UUID}/`, filename)) ||
          updateContentPack
        ) {
          console.log(
            "Content pack file",
            filename,
            "not found. Storing it..."
          );
          if (!this.limitedMode) {
            await fs.writeFile(`content/${UUID}/${filename}`, zipBlob);
          }
        }

        const synopsis = await getSynopsis(contentPack);

        return {
          ...dependency,
          name: synopsis.name,
          version: synopsis.version,
          size: formatBytes(zipBlob.size),
          state: CONTENT_PACK_STATE.STORED,
        };
      } catch (err) {
        console.log("Failed to load the zip file. Is it the right format?");
        console.log(err);
        return failedToLoadSynopsis;
      }
    },

    async tryLoadingDefinitionFile(contentPackName, entry) {
      this.currentLoadingSubstep = `Loading definition file '${entry.name}'...`;
      const definitionsObject = await getDefinitionFile(entry);
      // console.log(entry.name, JSON.parse(await entry.async("text")));

      this.tryLoadingExplorerProfiles(definitionsObject);
      this.tryLoadingHostileProfiles(definitionsObject);
      this.tryLoadingHostileGroups(definitionsObject);
      this.tryLoadingEncounterTypes(definitionsObject);
      this.tryLoadingTexts(contentPackName, entry.name, definitionsObject);
      this.tryLoadingStyles(definitionsObject);
    },

    tryLoadingExplorerProfiles(definitionsObject) {
      if (!definitionsObject.explorerProfiles) return;
      for (let profile of definitionsObject.explorerProfiles) {
        // If this is an inspired profile, and we can find a non-inspired profile, build the inspired by merging from the non inspired-profile
        if (
          profile.id.endsWith("__inspired") &&
          this.explorerProfiles[profile.id.slice(0, -10)]
        ) {
          profile = deepmerge(
            this.explorerProfiles[profile.id.slice(0, -10)],
            profile,
            overwriteMergeRule
          );
        }
        this.explorerProfiles[profile.id] = deepmerge(
          this.explorerProfiles[profile.id] ?? {},
          profile,
          overwriteMergeRule
        );
      }
    },

    tryLoadingHostileProfiles(definitionsObject) {
      if (!definitionsObject.hostileProfiles) return;
      for (const profile of definitionsObject.hostileProfiles) {
        this.hostileProfiles[profile.id] = deepmerge(
          this.hostileProfiles[profile.id] ?? {},
          profile,
          overwriteMergeRule
        );
        this.hostileGroups[profile.id] = deepmerge(
          this.hostileGroups[profile.id] ?? {},
          {
            id: profile.id,
            profiles: [profile.id],
          },
          overwriteMergeRule
        );
      }
    },

    tryLoadingHostileGroups(definitionsObject) {
      if (!definitionsObject.hostileGroups) return;
      for (const group of definitionsObject.hostileGroups) {
        this.hostileGroups[group.id] = deepmerge(
          this.hostileGroups[group.id] ?? {},
          group,
          overwriteMergeRule
        );
      }
    },

    tryLoadingEncounterTypes(definitionsObject) {
      if (!definitionsObject.encounterTypes) return;
      for (let encounterType of definitionsObject.encounterTypes) {
        this.encounterTypes[encounterType.id] = deepmerge(
          this.encounterTypes[encounterType.id] ?? {},
          encounterType,
          overwriteMergeRule
        );
      }
    },

    tryLoadingTexts(contentPackName, filename, definitionsObject) {
      if (!definitionsObject.texts) return;
      this.texts = deepmerge(
        this.texts,
        definitionsObject.texts,
        overwriteMergeRule
      );

      const fillTextMapping = (object, prefix = "") => {
        for (const property of Object.keys(object)) {
          const path = prefix + (prefix === "" ? "" : ".") + property;

          if (
            typeof object[property] === "string" ||
            typeof object[property] === "number"
          ) {
            this.textsMapping[path] = {
              contentPack: contentPackName,
              file: filename,
            };
          } else if (Array.isArray(object[property])) {
            fillTextMapping(
              Object.fromEntries(object[property].map((el, i) => [i, el])),
              path
            );
          } else {
            fillTextMapping(object[property], path);
          }
        }
      };

      fillTextMapping(definitionsObject.texts);
    },

    tryLoadingStyles(definitionsObject) {
      if (!definitionsObject.styles) return;
      this.styles = deepmerge(
        this.styles,
        definitionsObject.styles,
        overwriteMergeRule
      );
    },

    async tryLoadingImageFile(entry) {
      this.currentLoadingSubstep = `Loading image file '${entry.name}'...`;
      const name = getCleanFileNameFromZip(entry.name);
      this.images[name] = await getImageFileURL(entry); //"data:image/png;charset=utf-8;"+(await blobToBase64(imageBlob)).slice(30);//window.URL.createObjectURL(imageBlob);
    },
  },
});
