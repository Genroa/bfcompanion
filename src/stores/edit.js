import { defineStore, storeToRefs } from "pinia";
import * as fs from "src/lib/opfs";
import { overwriteMergeRule, useCompendium } from "./compendium";
import {
  getDefinitionFile,
  getImageFile,
  getImageFileURL,
  getSynopsis,
  listDefinitionFiles,
  listImageFiles,
  readContentPack,
} from "src/lib/contentpack";
import deepmerge from "deepmerge";

export const useEditStore = defineStore("edit", {
  state: () => ({
    selectedContentPack: null,
    selectedDefinitionFilename: null,
    selectedDefinitionFullName: null,
    dependency: null,
    files: {},
    filePaths: {},

    images: {},
    imagePaths: {},
    imageFiles: {},
  }),
  getters: {
    // This property computes the texts as if loaded into the compendium, so i18n can use it
    texts() {
      let texts = {};
      for (const file of Object.values(this.files)) {
        if (!file.texts) continue;
        texts = deepmerge(texts, file.texts, overwriteMergeRule);
      }
      return texts;
    },
    filesTree() {
      const flattenedNodes = [];
      // 1. Load the nodes
      for (const [fullname, path] of Object.entries(this.filePaths)) {
        const splitFullname = fullname.split("\\");
        flattenedNodes.push({
          label: splitFullname[splitFullname.length - 1],
          path: path.join("\\"),
          isFile: true,
          fullname,
          children: [],
        });

        // Create intermediary folders if they don't exist yet
        for (let i = 0; i < path.length; i++) {
          const folder = path[i];
          const folderPath = [...path].splice(0, i).join("\\");
          if (
            !flattenedNodes.find(
              (n) => n.path === folderPath && n.label === folder
            )
          ) {
            flattenedNodes.push({
              label: folder,
              fullname: path.join("\\"),
              path: folderPath,
              isFile: false,
              children: [],
            });
          }
        }
      }

      console.log("Nodes:", flattenedNodes);

      // 2. Set the parent of each node
      for (const node of flattenedNodes) {
        let parentPath = node.path.split("\\");
        let parentLabel = parentPath.pop();
        const parentFullname = parentPath.join("\\");
        // parentPath = parentPath.join("\\");

        const parentNode = flattenedNodes.find(
          (n) => n.path === parentFullname && n.label === parentLabel
        );
        if (parentNode) parentNode.children.push(node);

        console.log(
          "For node",
          node.path + "@" + node.label,
          "parent should be:",
          parentFullname + "@" + parentLabel,
          parentNode
        );
      }

      // 3. Keep parent-less nodes (roots)
      const nodes = flattenedNodes.filter((n) => n.path === "");
      return nodes;
    },
  },
  actions: {
    async selectContentPack(dependency) {
      const blob = new Blob([
        await fs.readFile(`content/${dependency.UUID}/${dependency.filename}`),
      ]);
      const contentPack = await readContentPack(blob);
      this.selectedContentPack = contentPack;
      this.dependency = dependency;
      console.log("Selecting content pack");
      for await (const entry of await listDefinitionFiles(contentPack)) {
        const path = entry.name.split(/\\|\//);
        const filename = path.pop();
        this.filePaths[entry.name] = path;
        this.files[entry.name] = await getDefinitionFile(entry);

        if (filename.includes("janus")) {
          console.log("JANUS:", entry.name, filename);
          console.log(this.files[filename]);
        }
      }

      for await (const entry of await listImageFiles(contentPack)) {
        const path = entry.name.split(/\\|\//);
        const filename = path.pop();
        this.imagePaths[filename] = path;
        this.images[filename] = await getImageFileURL(entry);
        this.imageFiles[filename] = await getImageFile(entry);
      }
    },
  },
});
